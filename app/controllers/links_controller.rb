class LinksController < ApplicationController
  before_action :set_link, only: %i[ show edit update destroy ]

  # GET /links or /links.json
  def index
    @links = Link.all
  end

  # GET /links/1 or /links/1.json
  def show
  end

  # GET /links/new
  def new
    @link = Link.new
  end

  # POST /links or /links.json
  def create
    @link = Link.new(link_params)
   respond_to do |format|
     if @link.save
     format.html { redirect_to '/links', notice: "Link was successfully created." }
     format.json { render :show, status: :created, location: @link }
     else
     format.html { render :new, status: :unprocessable_entity }
     format.json { render json: @link.errors, status: :unprocessable_entity }
     end
   end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def link_params
        params.fetch(:link, {}).permit(:url, :slug)
    end
end
