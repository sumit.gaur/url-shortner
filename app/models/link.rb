class Link < ApplicationRecord
  validates_presence_of :url
  validates_uniqueness_of :slug
  validates_length_of :url, within: 3..6000, on: :create, message: "too long"
  validates_length_of :slug, within: 3..6000, on: :create, message: "too long"
	before_validation :generate_slug
  
	def generate_slug
		slug = SecureRandom.uuid[0..5] 
		slug_exist = Link.where(slug: slug).first
		if slug_exist.present?
			self.slug =	slug + SecureRandom.uuid[0..2]
		else
			self.slug = slug
		end
	end


end
