# UrlShortner


# Technologies
  - Rails 6
  - Postgres

Project revisions are managed in **url-shortner** repository on private server.


### Installation

Url shortner requires [Ruby](https://www.ruby-lang.org/en/documentation/installation/) v2.7.2 to run.

```
Clone the git repository and install packages.
```sh
$ git clone "CLONE_URL"
$ cd url-shortner
$ bundle install
```
Setup database and start the rails.
```sh
$ rake db:create
$ rake db:migrate
$ rake db:seed
$ rails s
```
Visit the url [http:localhost:3000/links](http:localhost:3000/links)
