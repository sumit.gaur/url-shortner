class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string  :url, default: "",    null: false
      t.string :slug, default: "",    null: false
      t.timestamps
    end
  end
end
